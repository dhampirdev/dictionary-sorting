package dh.io;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Scanner;

public class File {
    /**
     * Builds and validates an input file path.
     *
     * @param filePath - path to input file.
     * @return validated path.
     * @throws IOException I/O error
     */
    public Path createInputPath(String filePath) throws IOException {
        Path path = Paths.get(filePath);

        if (!path.toFile().exists()) {
            throw new FileNotFoundException("Input file not found!");
        }

        if (!path.toFile().canRead()) {
            throw new IOException("Input file not readable!");
        }

        return path;
    }

    /**
     * Builds and validates an output file path.
     *
     * @param filePath - path to output file.
     * @return validate path.
     * @throws IOException I/O error
     */
    public Path createOutputPath(String filePath) throws IOException {
        Path path = Paths.get(filePath);

        if (!path.getParent().toFile().canWrite()) {
            throw new IOException("Output directory not writable!");
        }

        return path;
    }

    /**
     * Parses the contents of input file to a list of words.
     *
     * @param inputPath - path to input file.
     * @return parsed contents of input file as list of words.
     * @throws IOException I/O error
     */
    public ArrayList<String> parseInput(Path inputPath) throws IOException {
        ArrayList<String> result = new ArrayList<>();

        Scanner scanner = new Scanner(inputPath);
        while (scanner.hasNext()) {
            result.add(scanner.next());
        }

        scanner.close();

        return result;
    }

    /**
     * Composes output file from list of words.
     *
     * @param outputPath - path to output file.
     * @param items - words to output.
     * @throws IOException I/O error
     */
    public void composeOutput(Path outputPath, ArrayList<String> items) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath.toFile()));

        for (String item : items) {
            writer.write(item);
            writer.newLine();
        }

        writer.close();
    }
}
