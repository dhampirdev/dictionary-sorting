package dh.cmd;

public enum Args {
    INPUT_FILE("i", "input-file"),
    OUTPUT_FILE("o", "output-file"),
    STRATEGY_QUICK("Q", "quick"),
    STRATEGY_SELECTION("S", "selection");

    /**
     * Shorthand, custom value for this enumeration.
     */
    private String shortValue;

    /**
     * Full, custom value for this enumeration.
     */
    private String longValue;

    /**
     * Constructs this enumeration with custom value in two forms: short and long.
     *
     * @param shortValue - short, custom value for this enumeration.
     * @param longValue - long, custom value for this enumeration.
     */
    Args(String shortValue, String longValue) {
        this.shortValue = shortValue;
        this.longValue = longValue;
    }

    /**
     * @return String
     */
    public String getShortValue() {
        return shortValue;
    }

    @Override
    public String toString() {
        return longValue;
    }
}