package dh.cmd;

import org.apache.commons.cli.*;
import java.util.ArrayList;

public class Cmd {

    /**
     * Command name.
     */
    private static final String CMD_SYNTAX = "java -jar sort.jar";

    /**
     * Command-line arguments list.
     */
    private Options options;

    /**
     * Parsed command-line.
     */
    private CommandLine commandLine;

    /**
     * Constructs this command.
     *
     * Sets up command-line arguments.
     */
    public Cmd() {
        options = new Options();

        for (OptionDefinition definition : prepareOptionDefinitions()) {
            Option option = new Option(
                    definition.shortName,
                    definition.longName,
                    !definition.isBool,
                    definition.description
            );
            option.setRequired(definition.isRequired);

            options.addOption(option);
        }
    }

    /**
     * Prepares option definitions based on available argument enumerations.
     *
     * @return list of option definitions.
     */
    private ArrayList<OptionDefinition> prepareOptionDefinitions() {
        ArrayList<OptionDefinition> definitions = new ArrayList<>();

        definitions.add(new OptionDefinition(Args.INPUT_FILE, "path to input file", true, false));
        definitions.add(new OptionDefinition(Args.OUTPUT_FILE, "path to output file", true, false));
        definitions.add(new OptionDefinition(Args.STRATEGY_QUICK, "use quick algorithm (default)", false, true));
        definitions.add(new OptionDefinition(Args.STRATEGY_SELECTION, "use selection algorithm", false, true));

        return definitions;
    }

    /**
     * Parses command line input.
     *
     * @param args - list of command-line arguments as received by main method.
     */
    public void parse(String[] args) {
        CommandLineParser parser = new DefaultParser();
        try {
            commandLine = parser.parse(options, args);
        }
        catch(ParseException e) {
            throw new RuntimeException("Parse error - " + e.getMessage());
        }
    }

    /**
     * @return parse input file path.
     */
    public String getInputFile() {
        return commandLine.getOptionValue(Args.INPUT_FILE.toString());
    }

    /**
     * @return parse output file path.
     */
    public String getOutputFile() {
        return commandLine.getOptionValue(Args.OUTPUT_FILE.toString());
    }

    /**
     * @return is quick strategy selected
     */
    public boolean isQuickStrategy() { return commandLine.hasOption(Args.STRATEGY_QUICK.toString()); }

    /**
     * @return is selection strategy selected
     */
    public boolean isSelectionStrategy() { return commandLine.hasOption(Args.STRATEGY_SELECTION.toString()); }

    /**
     * Prints help message to standard output.
     */
    public void showHelp() {
        HelpFormatter help = new HelpFormatter();
        help.printHelp(CMD_SYNTAX, options, true);
    }

    /**
     * Encapsulates command-line option definition.
     */
    private class OptionDefinition {
        /**
         * Short notation.
         */
        String shortName;

        /**
         * Long notation.
         */
        String longName;

        /**
         * Full description.
         */
        String description;

        /**
         * Is this option required.
         */
        boolean isRequired;

        /**
         * Is this option a yes/no flag.
         */
        boolean isBool;

        /**
         * Constructs this option definition.
         *
         * @param argument - main part of definition, holds both notations.
         * @param description - full description.
         * @param isRequired - mark this option as required.
         * @param isBool - mark this option as yes/no flag.
         */
        OptionDefinition(Args argument, String description, boolean isRequired, boolean isBool) {
            shortName = argument.getShortValue();
            longName = argument.toString();
            this.description = description;
            this.isRequired = isRequired;
            this.isBool = isBool;
        }
    }
}
