package dh.sorting.factory;

import dh.sorting.algorithm.Algorithm;
import dh.sorting.algorithm.SelectionSort;

public class SelectionSortFactory<T extends Comparable<T>> implements AlgorithmFactory<T> {
    @Override
    public Algorithm<T> create() {
        return new SelectionSort<>();
    }
}
