package dh.sorting.factory;

import dh.sorting.algorithm.Algorithm;

/**
 * Contract for abstract sorting algorithm factory.
 */
public interface AlgorithmFactory<T extends Comparable<T>> {
    /**
     * Creates a concrete sorting algorithm.
     *
     * @return sorting algorithm implementation.
     */
    Algorithm<T> create();
}
