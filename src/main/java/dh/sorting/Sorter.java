package dh.sorting;

import dh.sorting.algorithm.Algorithm;
import dh.sorting.factory.AlgorithmFactory;
import dh.sorting.factory.QuickSortFactory;
import dh.sorting.factory.SelectionSortFactory;

import java.util.ArrayList;

public class Sorter<T extends Comparable<T>> {

    /**
     * Creates an algorithm factory depending on chosen strategy.
     *
     * @param strategy - selected sorting strategy.
     * @return sorting algorithm factory.
     * @throws Exception unsupported strategy.
     */
    private AlgorithmFactory<T> getAlgorithmFactory(Strategy strategy) throws Exception {
        switch (strategy) {
            case QUICK:
                return new QuickSortFactory<>();
            case SELECTION:
                return new SelectionSortFactory<>();
            default:
                throw new Exception("Unsupported strategy: " + strategy.toString());
        }
    }

    /**
     * Sorts given items using selected strategy.
     *
     * @param items - list of items to sort.
     * @param strategy - sorting algorithm.
     * @throws Exception unhandled error.
     */
    public void sort(ArrayList<T> items, Strategy strategy) throws Exception {
        AlgorithmFactory<T> factory = getAlgorithmFactory(strategy);
        Algorithm<T> algorithm = factory.create();

        algorithm.sort(items);
    }
}
