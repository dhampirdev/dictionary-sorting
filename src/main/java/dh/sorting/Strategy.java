package dh.sorting;

public enum Strategy {
    SELECTION, QUICK
}
