package dh.sorting.algorithm;

import java.util.ArrayList;

/**
 * Contract for sorting algorithm.
 */
public interface Algorithm<T extends Comparable<T>> {
    /**
     * Sorts given items.
     *
     * @param items - list of items to sort.
     */
    void sort(ArrayList<T> items);
}
