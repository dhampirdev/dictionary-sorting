package dh.sorting.algorithm;

import java.util.ArrayList;

/**
 * Selection Sort strategy.
 */
public class SelectionSort<T extends Comparable<T>> implements Algorithm<T> {
    @Override
    public void sort(ArrayList<T> items) {
        for (int i = 0; i < items.size(); i++) {
            int minIdx = i;
            for (int j = minIdx; j < items.size(); j++) {
                if (items.get(j).compareTo(items.get(minIdx)) <= 0) {
                    minIdx = j;
                }
            }

            T tmp = items.get(i);
            items.set(i, items.get(minIdx));
            items.set(minIdx, tmp);
        }
    }
}
