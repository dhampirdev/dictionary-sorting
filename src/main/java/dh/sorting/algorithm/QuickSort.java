package dh.sorting.algorithm;

import java.util.ArrayList;

/**
 * Quick Sort strategy.
 */
public class QuickSort<T extends Comparable<T>> implements Algorithm<T> {

    /**
     * List of items to sort.
     */
    private ArrayList<T> items = null;

    @Override
    public void sort(ArrayList<T> items) {
        this.items = items;
        quickSort(0, items.size() - 1);
        this.items = null;
    }

    /**
     * Prepares current partition of items for further partitioning.
     *
     * Groups lesser values in a left partition.
     * Groups bigger values in a right partition.
     * Establishes a new split index which separates lesser and bigger values.
     *
     * @param startIdx - starting index for current partition.
     * @param endIdx - ending index for current parition.
     * @return new split index.
     */
    private int partition(int startIdx, int endIdx) {
        T pivotValue = items.get(startIdx);

        int leftMark = startIdx + 1;
        int rightMark = endIdx;

        boolean done = false;
        while (!done) {

            while (leftMark <= rightMark && items.get(leftMark).compareTo(pivotValue) < 1) {
                leftMark++;
            }

            while (items.get(rightMark).compareTo(pivotValue) > -1 && rightMark >= leftMark) {
                rightMark--;
            }

            if (rightMark < leftMark) {
                done = true;
            } else {
                T tmp = items.get(leftMark);
                items.set(leftMark, items.get(rightMark));
                items.set(rightMark, tmp);
            }
        }

        items.set(startIdx, items.get(rightMark));
        items.set(rightMark, pivotValue);

        return rightMark;
    }

    /**
     * Helper method, executed recursively. Implements the quick sort strategy.
     *
     * @param startIdx - starting index for current partition.
     * @param endIdx - ending index for current partition.
     */
    private void quickSort(int startIdx, int endIdx) {
        if (startIdx < endIdx) {
            int splitPoint = partition(startIdx, endIdx);

            quickSort(startIdx, splitPoint - 1);
            quickSort(splitPoint + 1, endIdx);
        }
    }
}
