package dh;

import dh.cmd.Cmd;
import dh.io.File;
import dh.sorting.*;

import java.io.IOException;
import java.util.ArrayList;

public class App {
    /**
     * Command line wrapper.
     */
    private Cmd cmd;

    /**
     * Input/output file operations wrapper.
     */
    private File file;

    /**
     * Item sorter.
     */
    private Sorter<String> sorter;

    /**
     * Constructs this app.
     */
    private App() {
        cmd = new Cmd();
        file = new File();
        sorter = new Sorter<>();
    }

    /**
     * Maps CLI input to a sorting strategy enumeration.
     *
     * @return selected sorting strategy.
     */
    private Strategy mapSelectedStrategy() {
        Strategy selectedStrategy;
        if (cmd.isQuickStrategy()) {
            selectedStrategy = Strategy.QUICK;
        } else if (cmd.isSelectionStrategy()) {
            selectedStrategy = Strategy.SELECTION;
        } else {
            // Looks weird with just two options, but this is a default choice.
            selectedStrategy = Strategy.QUICK;
        }

        return selectedStrategy;
    }

    /**
     * Application's main entry point.
     *
     * @param args - raw command-line arguments
     */
    private void run(String[] args) {
        if (args.length == 0) {
            cmd.showHelp();
            return;
        }

        try {
            cmd.parse(args);

            ArrayList<String> items = file.parseInput(file.createInputPath(cmd.getInputFile()));
            Strategy strategy = mapSelectedStrategy();

            sorter.sort(items, strategy);

            file.composeOutput(file.createOutputPath(cmd.getOutputFile()), items);

        } catch (IOException ioException) {
            System.err.println("I/O Error: " + ioException.getMessage());
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        App app = new App();
        app.run(args);
    }

}
