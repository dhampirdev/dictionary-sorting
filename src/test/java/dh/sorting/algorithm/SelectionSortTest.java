package dh.sorting.algorithm;

public class SelectionSortTest extends BaseTest {
    @Override
    protected Algorithm<Integer> getIntegerAlgorithm() {
        return new SelectionSort<>();
    }

    @Override
    protected Algorithm<String> getStringAlgorithm() {
        return new SelectionSort<>();
    }
}
