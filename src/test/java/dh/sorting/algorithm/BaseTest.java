package dh.sorting.algorithm;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public abstract class BaseTest {

    protected abstract Algorithm<Integer> getIntegerAlgorithm();
    protected abstract Algorithm<String> getStringAlgorithm();

    @Test
    public void sortedNumbersRemainSorted() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);

        Algorithm<Integer> algorithm = getIntegerAlgorithm();
        algorithm.sort(numbers);

        Assert.assertArrayEquals(numbers.toArray(), new Integer[]{1, 2, 3, 4, 5});
    }

    @Test
    public void unsortedNumbersAreSortedNumericallyInAscendingOrder() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(4);
        numbers.add(1);
        numbers.add(3);
        numbers.add(5);
        numbers.add(2);

        Algorithm<Integer> algorithm = getIntegerAlgorithm();
        algorithm.sort(numbers);

        Assert.assertArrayEquals(numbers.toArray(), new Integer[]{1, 2, 3, 4, 5});
    }

    @Test
    public void sortedWordsRemainSorted() {
        ArrayList<String> words = new ArrayList<>();
        words.add("Aa");
        words.add("Ab");
        words.add("Ba");
        words.add("Bb");
        words.add("Ca");

        Algorithm<String> algorithm = getStringAlgorithm();
        algorithm.sort(words);

        Assert.assertArrayEquals(words.toArray(), new String[]{"Aa", "Ab", "Ba", "Bb", "Ca"});
    }

    @Test
    public void unsortedWordsAreSortedAsDictionary() {
        ArrayList<String> words = new ArrayList<>();
        words.add("Ca");
        words.add("Ba");
        words.add("Ab");
        words.add("Bb");
        words.add("Aa");

        Algorithm<String> algorithm = getStringAlgorithm();
        algorithm.sort(words);

        Assert.assertArrayEquals(words.toArray(), new String[]{"Aa", "Ab", "Ba", "Bb", "Ca"});
    }
}
