package dh.sorting.algorithm;

public class QuickSortTest extends BaseTest {
    @Override
    protected Algorithm<Integer> getIntegerAlgorithm() {
        return new QuickSort<>();
    }

    @Override
    protected Algorithm<String> getStringAlgorithm() {
        return new QuickSort<>();
    }
}
